package main

import (
	"log"
	"net/http"
)

type Service struct {
	Stack, Container, Traefik string
}

func NewService(r *http.Request) Service {

	if err := r.ParseForm(); err != nil {
		log.Printf("Error parsing form: %s", err)
	}

	s := Service{}

	s.Stack = r.Form.Get("stack")
	s.Container = r.Form.Get("container")
	s.Traefik = r.Form.Get("traefik") // trafeik hostname

	if c, err := r.Cookie("stack"); s.Stack == "" && err == nil {
		s.Stack = c.Value
	}

	if c, err := r.Cookie("container"); s.Container == "" && err == nil {
		s.Container = c.Value
	}

	if c, err := r.Cookie("traefik"); s.Traefik == "" && err == nil {
		s.Traefik = c.Value
	}

	return s
}

func (s *Service) Cookies() [2]*http.Cookie {

	return [2]*http.Cookie{
		&http.Cookie{Name: "stack", Value: s.Stack},
		&http.Cookie{Name: "container", Value: s.Container},
	}

}
