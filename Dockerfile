FROM blang/golang-alpine
MAINTAINER Juan Manuel Verges <jmanuel@cloudgeex.com>

WORKDIR /

COPY *.go src/

RUN go build -o proxy src/* && rm -r src
RUN chmod 555 proxy


CMD ["./proxy", "-url=http://104.197.32.190.xip.io:7080", "-domain=reviews", "-port=80"]

EXPOSE 80
