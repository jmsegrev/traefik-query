package main

import (
	"flag"
	"fmt"
	"log"
	"net/http"
	"net/url"
)

func main() {

	traefikURL := flag.String("url", "", "Traefik http proxy url")
	domain := flag.String("domain", "", "Traefik container domain")
	port := flag.Int("port", 7001, "Listening port")

	flag.Parse()

	if *traefikURL == "" || *domain == "" || *port == 0 {
		fmt.Println("-url, -domain and -port flags need to be set")
	}

	target, err := url.Parse(*traefikURL)
	if err != nil {
		panic(err)
	}

	if target.Scheme == "" {
		target.Scheme = "http"
	}

	proxy := NewProxy(*domain, target)
	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%d", *port), proxy))

}
