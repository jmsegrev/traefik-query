package main

import (
	"fmt"
	"net/http"
	"net/http/httputil"
	"net/url"
)

func NewProxy(domain string, target *url.URL) *Proxy {

	reverseProxy := httputil.NewSingleHostReverseProxy(target)
	director := reverseProxy.Director
	reverseProxy.Director = func(r *http.Request) {
		// use original director
		director(r)

		s := NewService(r)

		if s.Stack != "" && s.Container != "" {
			r.Host = fmt.Sprintf("%s.%s.%s", s.Container, s.Stack, domain)
		}
	}

	proxy := &Proxy{reverseProxy}

	return proxy
}

type Proxy struct {
	*httputil.ReverseProxy
}

func (p *Proxy) ServeHTTP(rw http.ResponseWriter, r *http.Request) {

	s := NewService(r)

	cookies := s.Cookies()
	for _, cookie := range cookies {
		http.SetCookie(rw, cookie)
	}

	p.ReverseProxy.ServeHTTP(rw, r)
}
